<!DOCTYPE html>

<html>

<head>
  
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url(); ?>___/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>___/css/style.css" rel="stylesheet">


  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>



<div class="container">

   <h3>Ubah Data Siswa</h3>

  <div class="table-responsive">

  <table class="table">

    <thead>

      <tr>

        <th></th>

        <th></th>

      </tr>

    </thead>

    <tbody>

      <form action="<?php echo base_url().'index.php/welcome/update';?>" method="post">

        <?php

        foreach ($data as $key => $value) { ?>

          <tr>

            <td width="100">Nama</td>

            <td width="300">

              <input type="hidden" class="form-control" value="<?php echo $value->id;?>" name="id" placeholder="Nama">

              <input type="text" class="form-control" value="<?php echo $value->nama;?>" name="nama" placeholder="Nama">

            </td>

          </tr>



          <tr>

            <td width="100">Mapel</td>

            <td width="300">

              <input type="text" class="form-control" value="<?php echo $value->mapel;?>" name="mapel" placeholder="Mapel">

            </td>

          </tr>



          <tr>

            <td width="100">Paket</td>

            <td width="300">

              <input type="text" class="form-control" value="<?php echo $value->paket;?>" name="paket" placeholder="Paket">

            </td>

          </tr>

        <?php

        }

        ?>

          <tr>

            <td colspan="2">

              <a href="<?php echo base_url().'index.php/welcome/';?>" class="btn btn-info">Batal</a>

              <button type="submit" class="btn btn-primary">Simpan</button>

            </td>

          </tr>

    </form>

    </tbody>

  </table>

  </div>

</div>
<center>
  <div class="ctr"> &copy; 2019 <a href="#">Aplikasi Ujian Online</a>. </div>

</center>


</body>

</html>