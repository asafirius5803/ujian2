<!DOCTYPE html>

<html>

<head>
  <title>USER</title>
 <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url(); ?>___/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>___/css/style.css" rel="stylesheet">


</head>

<body>
<!-- Menampilkan Jam (Aktif) -->
  <div id="clock"></div>
    <script type="text/javascript">
    <!--
    function showTime() {
        var a_p = "AM";
        var today = new Date();
        var curr_hour = today.getHours();
        var curr_minute = today.getMinutes();
        var curr_second = today.getSeconds();
       
        curr_hour = checkTime(curr_hour);
        curr_minute = checkTime(curr_minute);
        curr_second = checkTime(curr_second);
     document.getElementById('clock').innerHTML=curr_hour + ":" + curr_minute + ":" + curr_second + " " + a_p;
        }
 
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    setInterval(showTime, 500);
    //-->
    </script>
 
    <!-- Menampilkan Hari, Bulan dan Tahun -->
    
    <script type='text/javascript'>
      <!--
      var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
      var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
      var date = new Date();
      var day = date.getDate();
      var month = date.getMonth();
      var thisDay = date.getDay(),
          thisDay = myDays[thisDay];
      var yy = date.getYear();
      var year = (yy < 1000) ? yy + 1900 : yy;
      document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
      //-->
    </script>
  
  <div class="wrapper">
      <nav class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" >Aplikasi Ujian</a>
        
        </div>
      </nav>



<div class="container">

   <h3>Data User</h3>

  <a href="<?php echo base_url().'index.php/createadmin/tambah';?>" class="btn btn-primary">Tambah</a>
  <button onclick="history.go(-1);"><b>Kembali</b></button>
  
  <div class="table-responsive">

  <table class="table">

    <thead>

      <tr>

        <th>id</th>

        <th>username</th>

        <th>level</th>

        <th>password</th>

    <th>



    </th>

      </tr>

    </thead>

    <tbody>



    <?php

    $i = 1;

    foreach ($data as $key => $value) {

     ?>

     <tr>

      <td><?php echo $i;?></td>

          <td><?php echo $value->username;?></td>

          <td><?php echo $value->level;?></td>

          <td><?php echo $value->password;?></td>

      <td>

        <a href="<?php echo base_url().'index.php/createadmin/ubah/'.$value->id;?>" class="btn btn-info">Ubah</a>

        <a href="<?php echo base_url().'index.php/createadmin/hapus/'.$value->id;?>" class="btn btn-danger">Hapus</a>

      </td>

     </tr>

     <?php

     $i++;

    }

    ?>

    </tbody>

  </table>

  </div>

</div>
<div class="ctr"> &copy; 2019 <a href="#">Aplikasi Ujian Online</a>. </div>


</body>

</html>