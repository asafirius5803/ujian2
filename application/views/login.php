<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login</title>
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url(); ?>___/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>___/css/style.css" rel="stylesheet">
	</head>
	<body>
		<div class="container">
	<div class="col-md-4"></div>
	<div class="col-md-4">
			

			<form action="<?php echo base_url('index.php/login/user')?>" method="POST" >
				<div class="panel panel-default top150">
			<div class="panel-heading"><h4 style="margin: 5px"><i class="glyphicon glyphicon-user"></i> Silakan login</h4></div>
			<div class="panel-body">
				<div id="konfirmasi"></div>
				<div class="input-group">
					<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
					<input type="text" id="username" name="username" autofocus value="" placeholder="Username" class="form-control" />
				</div> <!-- /field -->
				
				<div class="input-group top15">
					<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
					<input type="password" id="password" name="password" value="" placeholder="Password" class="form-control"/>
				</div> <!-- /password -->
				<br>
				<div class="login-actions">
					<button class="btn btn-lg btn-primary btn-block	">Login</button>
					
				<center>
		<a href="<?php echo base_url().'index.php/createadmin/tambah';?>" class="text-center new-account">Create an account </a></center>
				</div> <!-- .actions -->
			</div>
		</div> <!-- /login-fields -->
		
<div class="ctr"> &copy; 2019 <a href="#">Aplikasi Ujian Online</a>. </div>
	</body>
</html>
				