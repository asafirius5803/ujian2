<html>
<head>
<title>Ilmu Pengetahuan Alam</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url(); ?>___/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>___/css/style.css" rel="stylesheet">

 <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>admin Template</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="style.css">
</head>
<body>
<!-- Menampilkan Jam (Aktif) -->
  <div id="clock"></div>
    <script type="text/javascript">
    <!--
    function showTime() {
        var a_p = "AM";
        var today = new Date();
        var curr_hour = today.getHours();
        var curr_minute = today.getMinutes();
        var curr_second = today.getSeconds();
       
        curr_hour = checkTime(curr_hour);
        curr_minute = checkTime(curr_minute);
        curr_second = checkTime(curr_second);
     document.getElementById('clock').innerHTML=curr_hour + ":" + curr_minute + ":" + curr_second + " " + a_p;
        }
 
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    setInterval(showTime, 500);
    //-->
    </script>
 
    <!-- Menampilkan Hari, Bulan dan Tahun -->
    
    <script type='text/javascript'>
      <!--
      var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
      var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
      var date = new Date();
      var day = date.getDate();
      var month = date.getMonth();
      var thisDay = date.getDay(),
          thisDay = myDays[thisDay];
      var yy = date.getYear();
      var year = (yy < 1000) ? yy + 1900 : yy;
      document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
      //-->
    </script>
  
<div class="wrapper">
      <nav class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" >Aplikasi Ujian</a>
        
        </div>
      </nav>
<center>
<font face=calibri size=6 color=black>
<b> MAPEL UJIAN Ilmu Pengetahuan Alam<br></font>
<font face=calibri size=4 color=blue>
<i>Kerjakan Dengan Jujur</i>
</font>
<i></i>
<br>
<button onclick="history.go(-1);"><i class="fa fa-caret-square-o-left"></i><b>Kembali</b></button>
</center>

<hr>
<aside class="sidebar">
        <menu>
          <ul class="menu-content">
          
           
             <h2> <a href="<?php echo base_url('index.php/IPApaket1/')?>"><i class="glyphicon glyphicon-pencil"></i> <span>Paket 1</span> </a></h2>
              
           
             <h2> <a href="<?php echo base_url('index.php/IPApaket2/')?>"><i class="glyphicon glyphicon-pencil"></i> <span>Paket 2</span> </a></h2>
              
            
             <h2> <a href="<?php echo base_url('index.php/IPApaket3/')?>"><i class="glyphicon glyphicon-pencil"></i> <span>Paket 3</span> </a></h2>
              
            
            
          </ul>
        </menu>
      </aside>
<div class="ctr"> &copy; 2019 <a href="#">Aplikasi Ujian Online</a>. </div>

</body>
</html>