<!DOCTYPE html>

<html>

<head>

<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url(); ?>___/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>___/css/style.css" rel="stylesheet">
  
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>IPA2</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="style.css">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>



<div class="container">

   <h3>SOAL Ilmu Pengetahuan Alam PAKET 2</h3>

  <a href="<?php echo base_url().'index.php/ipa2/tambah';?>" class="btn btn-primary">Tambah</a>
  <button onclick="history.go(-1);"><b>Kembali</b></button>

  <div class="table-responsive">

  <table class="table">

    <thead>

      <tr>

        <th>id_soal</th>

        <th>Soal</th>

        <th>a</th>

        <th>b</th>
        
        <th>c</th>
        
        <th>d</th>
        
        <th>knc_jawaban</th>

    <th>



    </th>

      </tr>

    </thead>

    <tbody>



    <?php

    $i = 1;

    foreach ($data as $key => $value) {

     ?>

     <tr>

      <td><?php echo $i;?></td>

          <td><?php echo $value->soal;?></td>

          <td><?php echo $value->a;?></td>

          <td><?php echo $value->b;?></td>

          <td><?php echo $value->c;?></td>

          <td><?php echo $value->d;?></td>

          <td><?php echo $value->knc_jawaban;?></td>

      <td>

        <!-- <button type="button" class="btn btn-primary" value="Ubah"></button> -->

        <a href="<?php echo base_url().'index.php/ipa2/ubah/'.$value->id_soal;?>" class="btn btn-info">Ubah</a>

        <a href="<?php echo base_url().'index.php/ipa2/hapus/'.$value->id_soal;?>" class="btn btn-danger">Hapus</a>

      </td>

     </tr>

     <?php

     $i++;

    }

    ?>

    </tbody>

  </table>

  </div>

</div>
<center>
  
<div class="ctr"> &copy; 2019 <a href="#">Aplikasi Ujian Online</a>. </div>


</center>


</body>

</html>