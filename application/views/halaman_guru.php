<!DOCTYPE html>
<html>
<head>
	
<meta name="apple-mobile-web-app-capable" content="yes">
<link href="<?php echo base_url(); ?>___/css/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>___/css/style.css" rel="stylesheet">
	
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>admin Template</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Anton" rel="stylesheet">  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <link rel="stylesheet" href="style.css">
</head>
<body>
 <!-- Menampilkan Jam (Aktif) -->
  <div id="clock"></div><span class="fa fa-calendar"></span>
    <script type="text/javascript">
    <!--
    function showTime() {
        var a_p = "AM";
        var today = new Date();
        var curr_hour = today.getHours();
        var curr_minute = today.getMinutes();
        var curr_second = today.getSeconds();
        curr_hour = checkTime(curr_hour);
        curr_minute = checkTime(curr_minute);
        curr_second = checkTime(curr_second);
     document.getElementById('clock').innerHTML=curr_hour + ":" + curr_minute + ":" + curr_second + " " + a_p;
        }
 
    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
    setInterval(showTime, 500);
    //-->
    </script>
 
    <!-- Menampilkan Hari, Bulan dan Tahun -->
    
    <script type='text/javascript'>
      <!--
      var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
      var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
      var date = new Date();
      var day = date.getDate();
      var month = date.getMonth();
      var thisDay = date.getDay(),
          thisDay = myDays[thisDay];
      var yy = date.getYear();
      var year = (yy < 1000) ? yy + 1900 : yy;
      document.write(thisDay + ', ' + day + ' ' + months[month] + ' ' + year);
      //-->
    </script>
 
<div class="wrapper">
      <nav class="navbar navbar-default">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" >Aplikasi Ujian</a>
        
        </div>
         <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span> Guru</a></li>
        <li><a href="<?php echo base_url(); ?>Login/logout" onclick="return confirm('keluar..?');"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
      </ul>
      </nav>
      <center>
<font face=ravie size=6>
<h1><b>Selamat Datang Di Halaman Guru<br></h1> 
<font face=calibri size=4 color=blue>
<i></i>
</center>
      <aside class="sidebar">
        <menu>
          <ul class="menu-content">
            <li>
              <h3><a href="#"><i class="fa fa-home"></i> Home</a></li></h3>
            <li>
             <h3> <a href="<?php echo base_url('index.php/Createsoal/')?>"><i class="fa fa-edit"></i> <span>Membuat Soal</span> </a></h3>
              
            </li>
            <li>
             <h3> <a href="<?php echo base_url('index.php/laporanhasil/')?>"><i class="fa fa-bar-chart"></i> <span>Laporan Hasil Ujian</span> </a></h3>
              
            </li>
            <li>
             <h3> <a href="<?php echo base_url('index.php/Welcome/')?>"><i class="fa fa-group"></i> <span>Lihat Data Siswa</span> </a></h3>
              
            </li>
            
          </ul>
        </menu>
      </aside>
    </div>

<div class="ctr"> &copy; 2019 <a href="#">Aplikasi Ujian Online</a>. </div>


</body>
</html>