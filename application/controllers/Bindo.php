<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Bindo extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();

  // load helper url dan model

  $this->load->model('bindomodel');

  $this->load->helper('url');

 }



 public function index()

 {

  // panggil fungsi model

  $result ['data'] = $this->bindomodel->get_data();

  // menampilkan view welcome_message.php dengan parameter $result data siswa

  $this->load->view('Bahasa_indonesia/bindotampil', $result);

 }



 public function tambah(){

  // menampilkan view tambah_soal.php

  $this->load->view('Bahasa_indonesia/tambah_soal');

 }



 public function save(){

  // menangkap data POST dari form tambah_siswa.php

  $datapos = $this->input->post();

  // memanggil model untuk simpan data dengan parameter data post

  $data = $this->bindomodel->save_data($datapos);



  redirect( base_url() . 'index.php/bindo');

 }



 function ubah(){

  // mengambil value segment 3 dari url, id_siswa

  $id = $this->uri->segment(3);

  $data = $this->bindomodel->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('Bahasa_indonesia/ubah_soal', $result);

 }



 public function update(){

  $datapos = $this->input->post();

  $data = $this->bindomodel->update_data($datapos);



  // memanggil controller index untuk kembali mengampilkan data table

  redirect( base_url() . 'index.php/bindo');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->bindomodel->hapus_data($id);

  

  // memanggil controller index untuk kembali mengampilkan data table

  redirect( base_url() . 'index.php/bindo');

 }



}
