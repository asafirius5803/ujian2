<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class IPA extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();

  // load helper url dan model

  $this->load->model('ipamodel');

  $this->load->helper('url');

 }



 public function index()

 {

  // panggil fungsi model

  $result ['data'] = $this->ipamodel->get_data();

  // menampilkan view welcome_message.php dengan parameter $result data siswa

  $this->load->view('IPA/ipatampil', $result);

 }



 public function tambah(){

  // menampilkan view tambah_soal.php

  $this->load->view('IPA/tambah_soal');

 }



 public function save(){

  // menangkap data POST dari form tambah_siswa.php

  $datapos = $this->input->post();

  // memanggil model untuk simpan data dengan parameter data post

  $data = $this->ipamodel->save_data($datapos);



  redirect( base_url() . 'index.php/ipa');

 }



 function ubah(){

  // mengambil value segment 3 dari url, id_siswa

  $id = $this->uri->segment(3);

  $data = $this->ipamodel->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('IPA/ubah_soal', $result);

 }



 public function update(){

  $datapos = $this->input->post();

  $data = $this->ipamodel->update_data($datapos);



  // memanggil controller index untuk kembali mengampilkan data table

  redirect( base_url() . 'index.php/ipa');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->ipamodel->hapus_data($id);

  

  // memanggil controller index untuk kembali mengampilkan data table

  redirect( base_url() . 'index.php/ipa');

 }



}
