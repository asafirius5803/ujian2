<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Createsoal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
			
	}
	public function index()
	{
		$this->load->view("buatsoal/buatsoal");

	}
	public function MTK()
	{
		$this->load->view("buatsoal/MTKtampil");
    }
	public function BIndo()
	{
		$this->load->view("buatsoal/BIndotampil");
    }
	public function IPA()
	{
		$this->load->view("buatsoal/IPAtampil");
    }
    public function Bhsinggris()
	{
		$this->load->view("buatsoal/Bhsinggristampil");
    }
    	

}