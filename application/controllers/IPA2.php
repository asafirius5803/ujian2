<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class IPA2 extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();

  // load helper url dan model

  $this->load->model('ipamodel2');

  $this->load->helper('url');

 }



 public function index()

 {

  // panggil fungsi model

  $result ['data'] = $this->ipamodel2->get_data();

  // menampilkan view welcome_message.php dengan parameter $result data siswa

  $this->load->view('IPA2/ipatampil2', $result);

 }



 public function tambah(){

  // menampilkan view tambah_soal.php

  $this->load->view('IPA2/tambah_soal');

 }



 public function save(){

  // menangkap data POST dari form tambah_siswa.php

  $datapos = $this->input->post();

  // memanggil model untuk simpan data dengan parameter data post

  $data = $this->ipamodel2->save_data($datapos);



  redirect( base_url() . 'index.php/ipa2');

 }



 function ubah(){

  // mengambil value segment 3 dari url, id_siswa

  $id = $this->uri->segment(3);

  $data = $this->ipamodel2->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('IPA2/ubah_soal', $result);

 }



 public function update(){

  $datapos = $this->input->post();

  $data = $this->ipamodel2->update_data($datapos);



  // memanggil controller index untuk kembali mengampilkan data table

  redirect( base_url() . 'index.php/ipa2');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->ipamodel2->hapus_data($id);

  

  // memanggil controller index untuk kembali mengampilkan data table

  redirect( base_url() . 'index.php/ipa2');

 }



}
