<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Bindopaket2 extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('bindopaket2siswamodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->bindopaket2siswamodel->get_data();


  $this->load->view('soal/bpaket2', $result);

 }



}
