<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Buatsoal extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();

  // load helper url dan model

  $this->load->model('buatsoalmodel');

  $this->load->helper('url');

 }



 public function index()

 {

  // panggil fungsi model

  $result ['data'] = $this->buatsoalmodel->get_data();

  // menampilkan view welcome_message.php dengan parameter $result data siswa

  $this->load->view('Matematika/buatsoaltampil', $result);

 }



 public function tambah(){

  // menampilkan view tambah_soal.php

  $this->load->view('Matematika/tambah_soal');

 }



 public function save(){

  // menangkap data POST dari form tambah_siswa.php

  $datapos = $this->input->post();

  // memanggil model untuk simpan data dengan parameter data post

  $data = $this->buatsoalmodel->save_data($datapos);



  redirect( base_url() . 'index.php/buatsoal');

 }



 function ubah(){

  // mengambil value segment 3 dari url, id_siswa

  $id = $this->uri->segment(3);

  $data = $this->buatsoalmodel->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('Matematika/ubah_soal', $result);

 }



 public function update(){

  $datapos = $this->input->post();

  $data = $this->buatsoalmodel->update_data($datapos);



  // memanggil controller index untuk kembali mengampilkan data table

  redirect( base_url() . 'index.php/buatsoal');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->buatsoalmodel->hapus_data($id);

  

  // memanggil controller index untuk kembali mengampilkan data table

  redirect( base_url() . 'index.php/buatsoal');

 }



}
