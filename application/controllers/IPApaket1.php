<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class IPApaket1 extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipapaket1siswamodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->ipapaket1siswamodel->get_data();


  $this->load->view('soal/ipaket1', $result);

 }



}
