<?php
class Login extends CI_Controller {
 
	public function __construct()
	{
		parent::__construct();
		$this->load->helper("url");
		$this->load->model('Login_model');
	}
 
	public function index() 
	{
		$this->load->view("login");
	}
 
	public function user()
	{
		$data['login'] = $this->Login_model->login();
		$this->load->view('user', $data);
	}

	public function logout() {
		$data = array(
                    'id' 		=> "",
                    'username' 	=> "",
                    'level' 	=> "",
                    'password' 	=> "",
                    
                    );
        $this->load->view('login',$data);
	}
}
