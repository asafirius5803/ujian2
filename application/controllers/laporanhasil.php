<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporanhasil extends CI_Controller {

 public function __construct() {
 parent::__construct();
 $this->load->model('laporan_model');
 }

public function index() {
 $data = array( 'title' => 'Data Nilai',
 'user' => $this->laporan_model->listing());
 $this->load->view('Hasil',$data);
 }

}

