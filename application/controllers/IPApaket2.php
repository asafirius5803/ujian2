<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class IPApaket2 extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('ipapaket2siswamodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->ipapaket2siswamodel->get_data();


  $this->load->view('soal/ipaket2', $result);

 }



}
