<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Createadmin extends CI_Controller {



 function __construct(){

  parent::__construct();

    $this->CI = & get_instance();


  $this->load->model('createusermodel');

  $this->load->helper('url');

 }



 public function index()

 {


  $result ['data'] = $this->createusermodel->get_data();


  $this->load->view('createuser/createuser_tampil', $result);

 }



 public function tambah(){

  $this->load->view('createuser/tambah_user');

 }



 public function save(){

  $datapos = $this->input->post(); /*$_POST[]*/

  // var_dump($datapos); die('aa');

  $data = $this->createusermodel->save_data($datapos);


  redirect( base_url() . 'index.php/createadmin');

 }



 function ubah(){

  $id = $this->uri->segment(3);

  $data = $this->createusermodel->get_dataByid($id);

  $result['data'] = $data;

  $this->load->view('createuser/ubah_user', $result);

 }



 public function update(){

  $datapos = $this->input->post();

  $data = $this->createusermodel->update_data($datapos);

  redirect( base_url() . 'index.php/createadmin');

 }



 function hapus(){

  $id = $this->uri->segment(3);



  $query = $this->createusermodel->hapus_user($id);

  redirect( base_url() . 'index.php/createadmin');

 }



}
