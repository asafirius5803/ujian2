-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2019 at 10:15 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ujian`
--

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `Nama` varchar(25) NOT NULL,
  `Matematika` varchar(25) NOT NULL,
  `Bahasa_Indonesia` varchar(25) NOT NULL,
  `IPA` varchar(25) NOT NULL,
  `Peringkat` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`Nama`, `Matematika`, `Bahasa_Indonesia`, `IPA`, `Peringkat`) VALUES
('aku', '80', '0', '90', '');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `mapel` varchar(30) NOT NULL,
  `paket` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id`, `nama`, `mapel`, `paket`) VALUES
(2, 'Budi', 'Bahasa indonesia', '1'),
(3, 'Putri', 'Matematika', '1'),
(4, 'Bagas', 'IPA', '3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalbhsindo1`
--

CREATE TABLE `tbl_soalbhsindo1` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalbhsindo1`
--

INSERT INTO `tbl_soalbhsindo1` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, 'Bacalah surat undangan berikut ini!\r\nYth. Saudara Bambang Sukadi\r\nJalan Sidobali 25\r\nKota Gede, Yogyakarta\r\n…\r\nhari : Senin\r\ntanggal : 20 Agustus 2004\r\nwaktu : pukul 08.30 sampai selesai\r\ntempat : ruang sidang SMA Pertiwi\r\n\r\nKami mengucapkan terima kasih atas perhatian saudara.\r\n\r\nKalimat yang tepat untuk melengkapi bagian pembuka surat di atas adalah …', ' Bersama surat ini kami mengun', 'Dengan surat ini kami mengunda', 'Kami mengundang Saudara untuk ', 'Kami mengundang Saudara hadir ', 'b'),
(2, 'Bacalah teks berikut! Penduduk desa binaan PKK provinsi mulai membajak sawah. Mereka akan menanam padi karena musim hujan sudah hadir. Penggunaan kata yang tidak tepat pada paragraf di atas adalah ….', 'Binaan', 'Membajak', 'Musim', 'Hadir', 'd'),
(3, 'Bacalah kutipan cerpen berikut! Aku bersyukur kepada Tuhan karena dia telah berubah. Aku pun memaafkannya, meskipun sampai saat ini aku belum bertemu dia lagi. Aku berharap suatu hari nanti kami akan menjalin persahabatan lagi. Penggalan cerpen di atas merupakan bagian ….', 'Krisis', 'Resolusi', 'Orientasi', 'Komplikasi', 'b'),
(4, 'Mudah mencampuri urusan orang lain merupakan perbuatan tidak terpuji. Dan kitaakan tersisih apabila orang lain tersebut bersanak saudara.Peribahsa yang tepat sesuai dengan pernyataan diatas adalah…', 'Air sama air kelak menjadi sat', 'Seperti biduk dikayuh hilir', 'Kucing lalu, tikus tidak berde', 'Terkusut-kusut sebagai anak ti', 'a'),
(5, 'Bacalah larik-larik puisi berikut!\r\n\r\nBuah mengkudu kusangka kandis\r\nKandis terletak dalam puan\r\nGula madu kusangka manis\r\n……….\r\n\r\nLarik yang tepat untuk melengkapi pantun di atas adalah………', 'Senyum adinda memang manis', 'Gula manis di dalam cawan', 'Kawan manis idaman hat', 'Manis lagi senyummu, Tuan', 'd'),
(6, ' Bacalah kutipan karya tulis berikut ini!\r\nManusia dalam kehidupannya tidak dapat melepaskan diri dari teknologi, baik yang sederhana maupun yang mutakhir. Pada era modernisasi, teknologi memberikan sumbangan yang begitu besar. Namun, banyak pula akibat buruk yang diperoleh dari dampak teknologi. Oleh karena itu, perlu diciptakan teknologi yang manusiawi, yaitu teknologi yang mampu mengembangkan harkat dan martabat manusia.\r\n\r\nDalam karya tulis ilmiah, uraian di atas merupakan bagian …', 'Tujuan penulisan', 'Landasan teori', 'Latar belakang masalah', 'Pembatasan masalah', 'c'),
(7, 'Bacalah penggalan puisi berikut dengan saksama!\r\n\r\nTeratai\r\nKepada Ki Hajar Dewantara\r\n\r\nDalam kebun di tanah airku\r\n…….\r\nTersembunyi kembang indah permai\r\nTidak terlihat orang yang lalu\r\nAkarnya tumbuh di hati dunia\r\nDaun bersemi Laksmi mengarang\r\nBiarpun ia diabaikan orang\r\nSeroja kembang gemilang mulia\r\n(Sanusi Pane)\r\nMajas alegori yang tepat untuk melengkapi puisi diatas adalah….', 'Tumbuh sekuntum bunga teratai', 'Lahirlah bunga bangsaku', 'Bunga bangsa berguguran', 'Ditanami bunga seroja', 'a'),
(8, ' (1) Dapat pula dikemukakan bahwa dalam paragraf yang kohesif tidak terdapat\r\nkalimat yang saling bertentangan. (2) Kohesif bermakna kepaduan. (3) Paragraf yang kohesif adalah paragraf yang hubungan antar kalimatnya padu atau berjalinan erat. (4) Kepaduan itu ditandai dengan terciptanya saling mendukung antara kalimat yang satu dengan kalimat yang lainnya. (5) Lebih jelas lagi dapat dikatakan bahwa paragaraf yang kohesif ditandai dengan tidak terjadinya saling mengingkari antara kalimat satu dengan kalimat lainnya.\r\nKalimat-kalimat tersebut akan menjadi paragraf yang padu apabila disusun dengan\r\nurutan…', '(2), (3), (5), (4), (1)', '(1), (3), (5), (4), (2)', '(5), (3), (2), (4), (1)', '(2), (3), (4), (5), (1)', 'd'),
(9, '(1) Dengarkan angin mengusir batang-batang padi.\r\n(2) Sebelum matahari terbenam.\r\n(3) Dengarkan senandung di balik jendela.\r\n(4) Rumah kecil, bukanlah pintu pagarku\r\n(5) Angin datang mengantarkan berita\r\n\r\nBerdasarkan puisi tersebut larik yang bermajas sama dintadai dengan nomor …..', '(1) dan (2)', '(1) dan (3)', '(1) dan (2)', '(1) dan (5)', 'd'),
(10, 'Semburan Baru Muncul di Mindi\r\n\r\nSemburan lumpur, air, dan gas baru keluar dari halaman belakang rumah salah seorang penduduk, warga Desa Mindi, Kecamatan Porong, Kabupaten Sidoarjo. Semburan itu merupakan semburan ke-59 yang muncul di sekitar pusat semburan utama.\r\nMenurut seorang ahli dan Leader Team Fergaco, perusahaan yang mengawasi gas-gas berbahaya di sekitar pusat semburan, semburan itu sama dengan 58 semburan liar sebelumnya. Semburan liar itu juga tidak berbahaya dan tidak akan membesar. Kalau dibiarkan semburan itu akan mengecil sendiri. Untuk menutup semburan, hari ini akan dimasukkan 100 kilogram semen ke dalam lubang asal semburan.\r\n\r\n Ide pokok paragraf kedua teks tersebut yang tepat adalah ....', 'Pengawasan gas oleh tim ahli.', 'Pendapat tentang semburan liar', 'Munculnya semburan liar.', 'Mengecilnya semburan liar.', 'c');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalbhsindo2`
--

CREATE TABLE `tbl_soalbhsindo2` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalbhsindo2`
--

INSERT INTO `tbl_soalbhsindo2` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, '(1) Objek wisata Pangandaran menyediakan transportasi rekreasi untuk memudahkan wisatawan menikmati keindahan pantai. (2) Wisatawan domestik maupun mancanegara dapat menggunakan transportasi untuk menikmati keindahan alam. (3) Di sepanjang tepi Pantai Pangandaran terlihat berjejer perahu untuk disewakan. (4) Dengan biaya Rp 1.000,00 saja per orang, para wisatawan dapat berputar di sekitar pantai dan menikmati keindahaan taman laut. (5) Para pedagang ikut meramaikan situasi pantai Pangandaran.\r\n\r\nKalimat utama paragraf tersebut terdapat pada nomor ...', '(1)', '(2)', '(3)', '(4)', 'a'),
(2, 'Banyak iklan yang menawarkan jasa untuk menurunkan berat badan. Ada yang melalui tusuk jarum, sedot lemak, minum jamu, atau minum teh hijau. Jenis diet pun bermacam-macam. Ada diet macan yang lebih mementingkan konsumsi daging. Ada diet buah-buahan membatasi makan nasi dan makanan berkarbohidrat tinggi. Beragam kiat dapat dilakukan untuk menurunkan berat badan ....\r\nSimpulan yang tepat untuk melengkapi paragraf tersebut adalah ....', 'Kita tidak akan mengalami kesu', 'Kaum wanita berlomba-lomba mel', 'Para remaja di kota-kota besar', 'Banyaknya iklan penawaran diet', 'd'),
(3, '(1) Dalam upaya pencegahan pencemaran udara, hutan mampu menangkal  polutan gas ataupun butiran padat. (2) Hasil penelitian menunjukkan bahwa volume udara yang mengandung polusi gas zon sebesar 150 ppm gas ternyata 99% terserap oleh tegukan hutan dalam waktu delapan jam. (3) Komplek industri  yang mengeluarkan polutan belerang dioksida di Uni Rusia ternyata berkurang dengan adanya jalur vegetasi kayu selebar 500 m yang mengelilingi kawasan industri tersebut. (4) Tumbuhan berkayu ataupun pohon memang diandalkan dalam penyelamatan keadaan lingkungan seperti tanah, air, dan udara walaupun peran pohon tersebut sebatas pada lingkungan, yang belum akut. (5) Pohon memang tidak akan mampu menetralisasi polusi, terutama pada kawasan industri besar.\r\n \r\nKalimat yang berisi fakta terdapat pada nomor…', '(1) dan (2)', '(1) dan (5)', ' (2) dan (3)', '(3) dan (4)', 'c'),
(4, 'Sepak bola adalah cabang olah raga yang telah merakyat di kalangan masyarakat. Permainan olah raga tersebut telah dikenal oleh setiap orang. Sepak bola bukan hanya kegemaran kaum lelaki, kaum wanita juga banyak yang menggemari olah raga ini. Ide pokok dari kutipan paragraf tersebut ialah …', 'Olahraga sepak bola', 'Kegemaran laki-laki', 'Keikutsertaan wanita', 'Sepak bola olah raga dunia', 'a'),
(5, 'Teks 1\r\nBadan Pengkajian dan Penerapan Teknologi (BPPT) mengemukakan teknologi reusable sanitary landfill. Teknologi itu ialah teknologi pengolahan sampah sanitary landfill khusus untuk sampah basah. Teknologi tersebut diharapkan mampu menghilangkan polusi udara.\r\nTeks 2\r\nSampah yang berasal dari rumah tangga setidaknya menyumbang sekitar 6000 ton total dari produksi sampah setiap harinya di ibukota Jakarta. Apabila setiap rumah tangga bisa mengelola sampah dengan baik, maka diharapkan dapat membantu mengatasi masalah sampah. Oleh karenanya, BPPT menggalakkan pengolahan sampah rumah tangga sehingga menjadi kompos yang bermanfaat untuk tumbuhan di dalam rumah.\r\n \r\n Persamaan informasi yang dibahas pada kedua teks tersebut ialah …', 'Sampah dan pengolahannya', 'Produksi sampah dan teknologi', 'Jakarta dan sampah', 'BPPT dan sampah', 'd'),
(6, 'Rindu Dendam\r\n\r\nAku mengembara seorang diri\r\nAntara bekas Majapahit\r\nAku bermimpi, terkenang dulu\r\nDan teringat waktu sekarang\r\n\r\nO, Dewata, pabila gerang\r\nAkan kembali kemegahan\r\nDan keindahan tanah airku?\r\nSanusi Pane\r\n\r\nAmanat yang ada pada puisi tersebut ialah …', 'Hidup harus menjadi seorang pe', 'Sudah selayaknya kita membaca ', 'Sukses adalah pilihan', 'Kita perlu belajar dengan kesu', 'd'),
(7, 'Angin bertiup sepoi-sepou. Cuaca begitu cerah cemerlang terkena sinar rembulan. Bintang-bintang bertaburan di atas langit seperti permata bertebaran di atas permadani. Di sebelah sana melancarlah biduk para nelayan yang sedang mengadu nasib dan untung, menentang besarnya gelombang yang penuh dengan marabahaya. Latar dari kutipan novel tersebut ialah …', 'Malam hari di langit biru', 'Malam hari di laut lepas', 'Pagi hari di samudera raya', 'Siang hari di tengah laut', 'b'),
(8, 'Selain unsur-unsur intrinsik drama, unsur lain yang penting untuk diperhatikan dalam pementasan adalah….', 'penonton', 'pentas', 'tiket masuk', 'daya Tarik tampung gedung', 'b'),
(9, ' Saudara-saudaraku, rasa optimisme untuk bersatu sangat dibutuhkan oleh bangsa kita tidak bisa memungkiri, disana sini kegagalan mental banyak ditemukan. Ragam penipuan, korupsi, kolusi, nepotisme bertebaran. Nah, apakah dengan kenyataan ini para pemuda kita akan tetap diam? Menuggu bom waktu meledak karena kesalahan bangsa sendiri?\r\nGagasan utama pidato diatas adalah….', 'makin marak ragam penipuan, ko', 'bangsa tengah menghadapi kemun', ' menanti bom waktu', 'ajaran untuk optimis', 'b'),
(10, 'Cara untuk menarik untuk mengawali pidato adalah…', 'mengungkapkan tema pidato', 'menjelaskan urutan sistematika', 'mengungkapkan fakta yang berhu', 'menjelaskan hal-hal yang berka', 'c');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalbhsindo3`
--

CREATE TABLE `tbl_soalbhsindo3` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalbhsindo3`
--

INSERT INTO `tbl_soalbhsindo3` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, 'Dalam buku Sri Sumarah (sebenarnya nama judul salah satu cerpen yang ada didalam buku tersebut), Umar Kayam seperti ingin menguak dunia yang kerap dihadapi oleh beberapa perempuan dalam berbagai situasi. Dengan sendirinya pengarang menciptakan karakter dan dunia keperempuanan pada suatui masa secara cerdas.\r\nHal yang dibahas dalam esai di atas adalah...', 'tema', 'amanat', 'latar', 'tokoh', 'd'),
(2, ' Berikut ini merupakan jenis esai, yaitu...', 'Proses', 'keagamaan', 'pendapat', 'perjalanan', 'c'),
(3, 'Saya menolak atau lebih tepatnya tidak menerima penuh bahwa puisi mesti padat, harus sedikit kata-kata. Daripada memenuhi syarat padat dan minimum tapi tidak indah serta gagap komunikasi, saya memilih puisi banyak kata tapi cantik, menyentuh, laju menghilir dan komunikatif. Puisi saya wajib musical. Kata-kata harus sedap didengar. Tentu saja kata-kata itu mengalami ketatnya seleksi.\r\nKutipan esai diatas termasuk jenis...', 'reflektif', 'argumentatif', 'proses', 'sejarah', 'b'),
(4, 'Daerah yang memiliki luasa 1.119,44 kilometer persegi dengan penduduk tak lebih dari satu jiwa ini sebenarnya memiliki potensi yang menarik. Selain memiliki pantai yang inda, Pacitan juga memiliki gua paling menarik di Asia Tenggara.\r\nTulisan di atas bukan esai, karena...', 'berisi pendapat penulis', 'lebih mengutamakan proses anal', 'mengarah kepada proses', 'mementingkan unsur berita', 'd'),
(5, 'Hubungan antara esai dengan artikel adalah...', 'menggunakan bahasa yang inform', 'menggunakan pendapat dan panda', 'menggunakan bahasa dengan gaya', 'tulisan sama-sama tepat sasara', 'b'),
(6, 'Mereka melaksanakan daripada kegiatan rutin yang sering dilaksanakan setiap tahun.\r\nBagian kalimat yang menyebabkan ketidak efektifan kalimat tersebut adalah...', 'mereka, daripada', 'kegiatan. Selalu', 'daripada, sering', 'daripada, yang', 'c'),
(7, 'Tubuh biru\r\nTatapan mata biru\r\nLelaki terguling di jalan\r\n      Lewat gardu belanda dengan bumi\r\nBerlindung warna malam\r\nSendiri masuk kota\r\nIngin ikut ngubur ibunya\r\nTema puisi tersebut adalah...', 'perlawanan     ', 'keberanian     ', 'kekejaman', 'perjuangan', 'd'),
(8, 'Bila kasih mu ibarat samudra\r\nSempit lautan tuduh\r\nTempat ku  mandi, mencuci lumut pada diri\r\nTempat ku berlayar, menebar pukat dan melempar sauh\r\nKalau aku ujian kemudian ditanya tentang pahlawan\r\nNama ku yang ku sebut paling dahulu\r\nLantaran ku tahu\r\nEngkau ibu dan aku anak mu.\r\n      \" Ibu \" karya D Zawawi Imron.\r\nIsi puisi tersebut menggambarkan...', ' Kesedihan seorang anak', 'Kegelisahan hati seorang anak', 'Perasaan rindu sreorang anak ', 'Perasaan sayang seorang anak ', 'd'),
(9, ' (1) Ketika itu pula ibu menceritakan bahwa kakanya Narothama lulus sebagai letnan muda dan dua minggu lagi akan diwisuda. (2). Suasana keluarga menjadi ceria mendengar kabar itu. Sambil bekerja tak henti-hentinya mereka memebicarakan Narothama. (3). Santi mewaili keluargana untuk menghadiri wisuda. (4). Malam keberangkatan santi tidak dapat tidur nyenak, banyak yang direncanakan dan yang diharapkannya.\r\nBukti suasana yang harap-harap cemas dan gelisah pada kutiban cerpen tersebut adalah pada kalimat nomor...', '(4)  ', '(3)  ', '(2)  ', '(1)  ', 'a'),
(10, 'Dadi, Darmo dan Linus, orag-orang yang paling dekat di hati mimin, kini sedang dikejar-kejar oleh penjahat. Pemerintah bukan lagi memimpin dan pelindung baginya.  Tapi semata mata penguasa. Penguasa yang kejam.\r\nKonflik yang dialami teman-teman Mimin dalam kutiban novel tersebut adalah konflik...', 'batin', 'psikis    ', 'fisik', 'ide', 'c');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalbhsinggris1`
--

CREATE TABLE `tbl_soalbhsinggris1` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalbhsinggris2`
--

CREATE TABLE `tbl_soalbhsinggris2` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalbhsinggris3`
--

CREATE TABLE `tbl_soalbhsinggris3` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalipa1`
--

CREATE TABLE `tbl_soalipa1` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalipa1`
--

INSERT INTO `tbl_soalipa1` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, 'Teratai dapat terapung di permukaan air karena mmepunyai...', 'Batang yang berongga', 'Akar yang panjang', 'Daun yang kecil', 'Duri yang tajam', 'a'),
(2, 'Bebek melakukan adaptasi berupa...', ' Memiliki bulu yang tipis', 'Mempunyai paruh yang tajam', 'Memiliki selaput pada kaki', 'Memiliki cakar pada kaki', 'c'),
(3, 'Kemampuan bunglon mengubah warna tubuhnya yaitu untuk...', 'Memudahkan berkembangbiak', 'Memudahkan mengendap menangkap', 'Menarik perhatian mangsa', 'Memudahkan memanjat pohon', 'a'),
(4, 'Unta memiliki punuk dipunggungnya yang berguna untuk...', 'Menarik perhatian betina', 'Menyimpan cadangan makanan dan', 'Melindungi diri dari musuh', 'Menarik penampilan di gurun', 'b'),
(5, 'Hewan yang melindungi diri dengan mengeluarkan bau yang menyengat yaitu..', 'Walangsangit', 'Gajah', 'Komodo', 'Jangkrik', 'a'),
(6, 'Contoh tumbuhan yang melakukan adaptasi untuk memperoleh makanan yaitu..', 'Kantung semar', 'Mawar', 'Kaktus', 'Teratai', 'a'),
(7, 'Fungsi paruh bebek yang lebar dan tipis yaitu...', 'Mengoyak daging', 'Meminum air sebanyak-banyaknya', 'Menyaring makanan dari air', 'Menyaring makanan dari air', 'c'),
(8, 'Landak melindungi diri dengan cara..', 'Sirip yang luas', 'Kulit duri yang tajam', 'Bau yang menyengat', 'Racun yang berbisa', 'b'),
(9, 'Beruang kutub dapat terlindung dari cuaca dingin karena mempunyai..', 'Bulu yang keras dan runcing', 'Lapisan lemak dan bulu tebal', 'Darah yang panas', 'Lapisan kulit yang tajam', 'b'),
(10, 'Hewan yang melumpuhkan mangsa dengan racun yang dimilikinya yaitu..', 'lipan dan musang', 'macan dan kalajengking', 'ular dan singa', 'ular dan kalajengking', 'd');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalipa2`
--

CREATE TABLE `tbl_soalipa2` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalipa2`
--

INSERT INTO `tbl_soalipa2` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, ' Pohon jati mengggugurkan daunnya dengan tujuan ....', 'Mempercepat pertumbuhan batang', 'Mengurangi penguapan', 'Menarik pemangsa', 'Meneduhkan daunnya', 'b'),
(2, 'Penyesuaian diri makhluk hidup terhadap lingkungan disebut....', 'Adaptasi', 'Habitat', 'Organisasi', 'Ekosistem', 'a'),
(3, 'Bentuk paruh yang dimiliki burung menyesuaikan ....', 'Jenis sarangnya ', ' Jenis bulunya', 'Jenis telurnya', 'Jenis makanannya', 'd'),
(4, 'Cicak memutuskan ekornya dengan tujuan ....', 'Menarik cicak betina', 'Menarik perhatian mangsanya', ' Mengelabuhi pemangsanya', 'Mudah berkembangbiak', 'c'),
(5, 'Bentuk taring dan cakar singa yang tajam bermanfaat untuk ....', 'Memanjat pohon', 'Berenang', 'Menggali tanah', 'Mencengkram mangsa', 'd'),
(6, 'Kemampuan bunglon merubah warna tubuhnya dinamakan ....', 'Reboisasi', 'Ekolokasi', 'Autotomi', 'Mimikri', 'd'),
(7, 'Menggugurkan daun ketika kemarau merupakan bentuk adaptasi dari tanaman berikut ini, kecuali ....', 'Randu', ' Kantung semar', 'Jati', 'Pohon Mahoni', 'b'),
(8, 'Di bawah ini ayang termasuk ciri-ciri yang dimiliki tumbuhan teratai yaitu ....', 'Mempunyai daun panjang menyiri', ' Mempunyai daun lebar tebal', 'Mempunyai daun berbentuk duri', 'Mempunyai daun tipis dan lebar', 'd'),
(9, 'Hewan-hewan carnivota diatas yang tidak mempunyai gigi yang tajam yaitu ...', 'Buaya', 'Elang', 'Singa', 'Kucing', 'b'),
(10, 'Burung mempunyai paruh dan bentuk kaki yang berbeda-beda sesuai dengan…', 'Jenis makanan dan tempat hidup', 'Bentuk makanannya', 'Keindahan tempatnya', 'Kelincahan mangsanya', 'a');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalipa3`
--

CREATE TABLE `tbl_soalipa3` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalipa3`
--

INSERT INTO `tbl_soalipa3` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, '  Tumbuhan insektivora merupakan tumbuhan pemakan ....', 'Serangga', 'Mamalia', ' Tumbuhan', 'Cicak', 'a'),
(2, ' Mempunyai daun yang lebar dan batang berongga adalah ciri-ciri penyesuaian diri tumbuhan...', 'Teratai', 'Kaktus', 'Mangga', 'Kembang sepatu', 'a'),
(3, 'Kupu-kupu mempunyai bentuk mulut...', 'Penusuk ', ' Penjilat', 'Penusuk dan penghisap', 'Penghisap', 'd'),
(4, 'Kaktus mempunyai akar yang panjang, untuk..', 'Bernafas', ' Membuat makanan', ' Mecari sumber air yang letakn', 'Menyimpan cadangan makanan', 'c'),
(5, 'Selaput pada kaki bebek bermanfaat untuk...', ' Bertengger di ranting pohon', 'Memenjat pohon', 'Mencengkram mangsanya', 'Berenang di air', 'd'),
(6, ' Ayam mempunyai alat bela diri untuk melukai musuhnya yang disebut ...', 'Mata', 'Jengger', 'Taji', 'Ekor', 'c'),
(7, 'Unta mampu bertahan tanpa minum air dalam waktu lama karena mempunyai....', 'Cadangan lemak yang dapat diub', ' Punuh yang kuat semar', 'Leher panjang', 'Kulit yang diselimuti rambut t', 'a'),
(8, ' Kambing, sapi, dan kerbau mempunyai alat bela diri berupa....', 'Sengat yang berbisa', ' Tanduk yang runcing', ' Ekor yang panjang', 'Duri yang tajam', 'b'),
(9, 'Tumbuhan yang tidak menggugurkan daunnya pada saat kemarau yaitu...', 'Pinus', 'Randu', 'Jati', 'Kedondong', 'd'),
(10, ' Kumbang badak mempunyai alat bela diri berupa..', 'Duri yang tajam', 'Jengger', 'Taji', 'Tanduk yang kuat', 'b');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalmtk1`
--

CREATE TABLE `tbl_soalmtk1` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalmtk1`
--

INSERT INTO `tbl_soalmtk1` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(8, 'Dua buah lingkaran dengan jarak kedua pusat lingkaran adalah 26 cm, memiliki jari-jari lingkaran besar 12 cm, dan jari-jari lingkaran kecil 2 cm. Maka, panjang garis singgung persekutuan luarnya adalah ...', '24 cm ', '42 cm', '60 cm', '82 cm', 'a'),
(7, 'Pada layar televisi, panjang sebuah mobil adalah 14 cm dan tingginya 4 cm. Jika tinggi sebenarnya adalah 1 m, maka panjang mobil sebenarnya adalah...', '3 m', '3,5 m', '4 m', '4,5 m', 'b'),
(5, 'Sebuah taman berbentuk persegi. Di sekeliling taman itu ditanami pohon cemara dengan jarak antar pohon 8 m. Panjang sisi taman itu adalah 78 m. Berapakah banyak pohon cemara yang dibutuhkan?', '30 buah\r\n', '35 buah', '39 buah', '49 buah', 'c'),
(4, 'Umur Ibu 4 tahun lebih muda dari umur Ayah. Jumlah umur Ibu dan Ayah adalah 78 tahun. Berapakah umur Ayah sekarang?', '30', '37', '41', '50', 'c'),
(3, 'Diberikan P = {1,2,3,4,5,6,7,8,9}. Himpunan bilangan ganjil yang terdapat di P adalah....', ' {1,2,3,7} ', ' {2,4,8,9}', ' {1,3,5,7,9}', '{1,5,7,9}', 'c'),
(6, 'Sebuah tangki air berbentuk tabung dengan jari-jari 60 cm dan tinggi 1,4 m akan diisi air sampai penuh. Banyaknya air yang mengisi tangki adalah..liter', '1.864 liter', '1.584 liter', '1.684 liter', '1.464 liter', 'b'),
(2, 'Anis membeli sebuah telfon genggam dengan harga Rp1.800.000,00, setelah pemakaian 3 bulan Anis menjual dengan harga Rp1.200.000,00. Berapa persentase kerugian yang dialami oleh Anis?', '23%', '33%', '34%', '35%', 'b'),
(1, 'Perbandingan panjang dan lebar persegi panjang adalah 4 : 3. Jika kelilingnya 84 cm, luasnya adalah..', '234 cm2', '324 cm2', '432 cm2', '452 cm2', 'c'),
(9, 'Sinta memiliki bak mandi berbentuk balok dengan tinggi 30 cm, lebarnya 60 cm dan panjang 100 cm. Bak tersebut akan diisi air. Banyak air yang dibutuhkan Sinta untuk mengiri 2/3 bagian bak mandi tersebut adalah ...', ' 210.000 cm3', '180.000 cm3', '160.000 cm3', '120.000 cm3', 'd'),
(10, 'Alas sebuah prisma berbentuk segitiga siku-siku dengan panjang 12 cm, 16 cm, dan 20 cm. Jika tinggi prisma 30 cm, maka volume prisma tersebut adalah... ', '987 cm3', '1.208 cm3', '2.880 cm3', '3.675 cm3', 'c');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalmtk2`
--

CREATE TABLE `tbl_soalmtk2` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalmtk2`
--

INSERT INTO `tbl_soalmtk2` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, 'Alas sebuah limas beraturan berbentuk persegi dengan panjang sisi 10 cm dan tinggi limas 12 cm, maka luas permukaan limas adalah...', '260 cm2', '340 cm2', '360 cm2', '620 cm2', 'c'),
(2, 'Pak Komar memiliki sebuah peternakan di mana di dalamnya ada 20 ekor kambing dan 40 ekor sapi. Pak Komar membawa 2 ekor sapi dan 1 ekor kambing untuk diperiksa kesehatannya ke dokter hewan. Dari ilustrasi tersebut, yang merupakan populasi adalah ...', ' Empat puluh ekor sapi', ' Dua puluh ekor kambing', 'Dua ekor sapi dan satu ekor ka', 'Seluruh hewan ternak', 'd'),
(3, 'Dua buah mata uang logam dilempar bersama-sama. Peluang munculnya dua angka adalah...', '0,20 ', '0,25', '0,50', '0,75', 'b'),
(4, 'Apabila a = 3; b = 0; dan c = -3, maka nilai dari  { a x (b + c – a)} x ( b + c ) = ...', ' -54  ', '-45', '45', '54', 'd'),
(5, ' Seorang siswa berhasil menjawab dengan benar 28 soal, salah 8 soal, serta tidak menjawab 4 soal. Bila satu soal dijawab benar nilainya 4, salah nilainya -3, serta tidak menjawab    nilainya  -1. maka nilai yang diperoleh siswa tersebut adalah...', '96  ', '88', '84', '91', 'c'),
(6, 'Jumlah dua bilangan pecahan saling berkebalikan adalah  34/15 . Jika salah satu penyebut bilangan itu adalah 5. salah satu bilangan tersebut adalah...', '2/5', '3/5', '5/4', '5/6', 'b'),
(7, ' Suatu jenis pupuk terdiri dari 50% ammonium, 35% super fosfat, dan sisanya besi sulfat. Jika berat pupuk tersebut 15 kg, maka berat besi sulfat adalah...', ' 22,5 gr ', ' 2.25 g', '2,25 kg', '22,5 kg', 'c'),
(8, 'Siswa SMP Andalan sebanyak 140 anak membentuk suatu barisan. Barisan pertama terdiri dari 5 anak. Dan setiap barisan berikutnya selalu bertambah 2 anak dari barisan sebelumnya. Banyak barisan yang dapat dibentuk oleh seluruh siswa tersebut adalah ... ', '6', '10', '11', '12', 'b'),
(9, 'Jika untuk mengecat dinding yang berukuran 6 m2 dibutuhkan 4 kaleng cat, maka untuk mengecat dinding yang berukuran 15 m2  dibutuhkan kaleng cat sebanyak ...', '10', '9', '8', '7', 'a'),
(10, 'Sebuah bangun ruang berbentuk tabung dengan tutup setengah bola mempunyai diameter 14 cm. Jika tinggi tabung tersebut adalah 20 cm, maka luas permukaan bangun ruang tersebut adalah ...', '1432 cm2', '1342 cm2', '1324 cm2', '1243 cm2', 'b');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_soalmtk3`
--

CREATE TABLE `tbl_soalmtk3` (
  `id_soal` int(5) NOT NULL,
  `soal` text NOT NULL,
  `a` varchar(30) NOT NULL,
  `b` varchar(30) NOT NULL,
  `c` varchar(30) NOT NULL,
  `d` varchar(30) NOT NULL,
  `knc_jawaban` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_soalmtk3`
--

INSERT INTO `tbl_soalmtk3` (`id_soal`, `soal`, `a`, `b`, `c`, `d`, `knc_jawaban`) VALUES
(1, 'Hasil -24+72:(-12)-2x(-3) adalah...', '-24 ', '-18', '18', '24', 'a'),
(2, 'Sebuah sekolah menyelenggarakan lomba matematika dengan aturan memjawab benar diberikan skor 5,tidak menjawab skornya 0, dan menjawab salah skornya -2. Dalam tes tersebut Andi berhasil menjawab benar 15 soal dan tidak menjawab 8 soal dari 30 soal yang diberikan. Skor Andi adalah...', '75       ', '72  ', '61 ', '59', 'c'),
(3, 'Perbandingan uang imran dan uang sadewa adalah 7:8. Jika jumlah mereka Rp2.400.000,00,maka selisih uang mereka adalah... ', 'Rp160.000,00', 'Rp210.000,00', 'Rp240.000,00', 'Rp315.000,00', 'a'),
(4, 'Nilai x yang memenuhi 2(x+1) + 3(2x-4)=14 adalah... ', '3', '4', '5', '6', 'a'),
(5, 'Jarak dua kota pada peta adalah 20 cm. Jika skala peta 1 : 600.000, jarak dua kota sebenarnya adalah..', '1.200 km', '120 km', '30 km', '12 km', 'd'),
(6, 'vega menyimpan uang di bank sebesar Rp2000.000,00 dengan suku bunga 18% setahun dengan bunga tunggal. Besarnya tabungan vega pada akhir bulan keenam adalah...', 'Rp2.180.000', 'Rp2.080.000', 'Rp2.160.000', 'Rp2.060.000', 'a'),
(7, 'Dalam membangun satu gudang yang sederhana dibutuhkan waktu selama 36 hari dan membutuhkan tenaga kerja sebanyak 12 orang. Berapakah waktu yang dibutuhkan untuk membangun satu gudang jika menggunakan tenaga kerja sebanyak dua puluh empat orang?', '12 hari', '14 hari', '16 hari', '18 hari', 'd'),
(8, 'Sebuah kereta api berangkat dari Surabaya menuju kota Jakarta pada jam 7.00 WIB , lama perjalanan yang di tempuh dari Surabaya ke Jakarta adalah selama 4 jam, namun ternyata kereta api tersebut transit di Yogyakarta selama setengah jam. Pada jam berapakah kereta tersebut tiba di Jakarta ?', '10.30 WIB', '10.45 WIB', '11.30 WIB', '11.45 WIB', 'c'),
(9, 'Dalam suatu kelas terdapat siswa sebanyak tiga puluh sembilan orang. Lima belas di antaranya adalah siswa yang menyukai pelajaran biologi, dua puluh delapan orang adalah siswa yang menyukai pelajaran fisika sedangkan enam orang siswa lainnya adalah siswa yang menyukai pelajaran biologi dan juga menyukai pelajaran fisika. Berapakah siswa yang tidak menyukai pelajaran biologi dan juga fisika ?', 'Dua orang', 'Tiga orang', 'Empat orang', 'Lima orang', 'a'),
(10, 'Seorang karyawan perpustakaan harus membawakan lima dus buku ke perpustakaan. Jika dus buku yang ada adalah sebanyak sembilan dus buku, berapa kali ia harus mengantarkan buku-buku tersebut jika ia hanya mampu mengangkat 2 dus buku dalam satu kali antar ?', '4', '5', '6', '7', 'b');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `level` int(11) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `level`, `password`) VALUES
(0, 'admin', 1, '21232f297a57a5a743894a0e4a801fc3'),
(0, 'siswa', 2, 'bcd724d15cde8c47650fda962968f102'),
(0, 'guru', 3, '77e69c137812518e359196bb2f5e9bb9'),
(0, 'orangtua', 4, '469eb28221c8e6d092ddafacb87799bf');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_soalbhsinggris1`
--
ALTER TABLE `tbl_soalbhsinggris1`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_soalbhsinggris2`
--
ALTER TABLE `tbl_soalbhsinggris2`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_soalbhsinggris3`
--
ALTER TABLE `tbl_soalbhsinggris3`
  ADD PRIMARY KEY (`id_soal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_soalbhsinggris1`
--
ALTER TABLE `tbl_soalbhsinggris1`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_soalbhsinggris2`
--
ALTER TABLE `tbl_soalbhsinggris2`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_soalbhsinggris3`
--
ALTER TABLE `tbl_soalbhsinggris3`
  MODIFY `id_soal` int(5) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
